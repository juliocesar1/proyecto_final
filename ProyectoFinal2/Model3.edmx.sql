
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/30/2015 11:17:23
-- Generated from EDMX file: c:\users\diegofernando\documents\visual studio 2013\Projects\ProyectoFinal2\ProyectoFinal2\Model3.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BDproyecto];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AlgunosProductoSet'
CREATE TABLE [dbo].[AlgunosProductoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [NombreProducto] nvarchar(max)  NOT NULL,
    [PresentacionProducto] nvarchar(max)  NOT NULL,
    [TipoProducto] nvarchar(max)  NOT NULL,
    [PrecioProducto] int  NOT NULL,
    [ImagenProducto] varbinary(max)  NOT NULL
);
GO

-- Creating table 'ServicioSet'
CREATE TABLE [dbo].[ServicioSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [NombreServicio] nvarchar(max)  NOT NULL,
    [DescripcionServicio] nvarchar(max)  NOT NULL,
    [PrecioServicio] nvarchar(max)  NOT NULL,
    [ImagenServicio] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PaginasSet'
CREATE TABLE [dbo].[PaginasSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Quienessomos] nvarchar(max)  NOT NULL,
    [Producto] nvarchar(max)  NOT NULL,
    [Contacto] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'AlgunosProductoSet'
ALTER TABLE [dbo].[AlgunosProductoSet]
ADD CONSTRAINT [PK_AlgunosProductoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ServicioSet'
ALTER TABLE [dbo].[ServicioSet]
ADD CONSTRAINT [PK_ServicioSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PaginasSet'
ALTER TABLE [dbo].[PaginasSet]
ADD CONSTRAINT [PK_PaginasSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------