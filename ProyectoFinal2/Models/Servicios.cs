﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoFinal2.Models
{
    public class Servicios
    {
        public int Id { get; set; }
        public string NombreServicio { get; set; }
        public string DescripcionServicio { get; set; }
        public string PrecioServicio { get; set; }
        public string ImagenServicio { get; set; }
    }
}