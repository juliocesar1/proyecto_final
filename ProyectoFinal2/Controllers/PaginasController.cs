﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoFinal2;

namespace ProyectoFinal2.Controllers
{
    public class PaginasController : Controller
    {
        private Model3Container db = new Model3Container();

        // GET: /Paginas/
        public ActionResult Index()
        {
            return View(db.PaginasSet.ToList());
        }

        // GET: /Paginas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paginas paginas = db.PaginasSet.Find(id);
            if (paginas == null)
            {
                return HttpNotFound();
            }
            return View(paginas);
        }

        // GET: /Paginas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Paginas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Quienessomos,Producto,Contacto")] Paginas paginas)
        {
            if (ModelState.IsValid)
            {
                db.PaginasSet.Add(paginas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(paginas);
        }

        // GET: /Paginas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paginas paginas = db.PaginasSet.Find(id);
            if (paginas == null)
            {
                return HttpNotFound();
            }
            return View(paginas);
        }

        // POST: /Paginas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Quienessomos,Producto,Contacto")] Paginas paginas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paginas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paginas);
        }

        // GET: /Paginas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paginas paginas = db.PaginasSet.Find(id);
            if (paginas == null)
            {
                return HttpNotFound();
            }
            return View(paginas);
        }

        // POST: /Paginas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Paginas paginas = db.PaginasSet.Find(id);
            db.PaginasSet.Remove(paginas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
