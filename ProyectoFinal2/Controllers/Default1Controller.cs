﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoFinal2;

namespace ProyectoFinal2.Controllers
{
    public class Default1Controller : Controller
    {
        private Model3Container db = new Model3Container();

        // GET: /Default1/
        public ActionResult Index()
        {
            return View(db.AlgunosProductoSet.ToList());
        }

        // GET: /Default1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlgunosProducto algunosproducto = db.AlgunosProductoSet.Find(id);
            if (algunosproducto == null)
            {
                return HttpNotFound();
            }
            return View(algunosproducto);
        }

        // GET: /Default1/Create
        public ActionResult Crear()
        {
            return View();
        }

        // POST: /Default1/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,NombreProducto,PresentacionProducto,TipoProducto,PrecioProducto,ImagenProducto")] AlgunosProducto algunosproducto)
        {
            if (ModelState.IsValid)
            {
                db.AlgunosProductoSet.Add(algunosproducto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(algunosproducto);
        }

        // GET: /Default1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlgunosProducto algunosproducto = db.AlgunosProductoSet.Find(id);
            if (algunosproducto == null)
            {
                return HttpNotFound();
            }
            return View(algunosproducto);
        }

        // POST: /Default1/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,NombreProducto,PresentacionProducto,TipoProducto,PrecioProducto,ImagenProducto")] AlgunosProducto algunosproducto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(algunosproducto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(algunosproducto);
        }

        // GET: /Default1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlgunosProducto algunosproducto = db.AlgunosProductoSet.Find(id);
            if (algunosproducto == null)
            {
                return HttpNotFound();
            }
            return View(algunosproducto);
        }

        // POST: /Default1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AlgunosProducto algunosproducto = db.AlgunosProductoSet.Find(id);
            db.AlgunosProductoSet.Remove(algunosproducto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
